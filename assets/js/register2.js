//target elements
const registerForm = document.getElementById('register');

//element.addEventListener("event", () => {}) //listens to any event made by user
registerForm.addEventListener("submit", (e) => {
	e.preventDefault() //prevent from refreshing the browser
	
	//console.log(e);
	//console.log(e.target.value)
	const fn = document.getElementById('firstname').value;
	const ln = document.getElementById('lastname').value;
	const email = document.getElementById('email').value;
	const pw = document.getElementById('password').value;
	const cpw = document.getElementById('cpw').value;

	// console.log(fn);
	// console.log(ln);
	// console.log(email);
	// console.log(pw);
	// console.log(cpw);

	//condition pw should match confirm pw
	if (pw === cpw){

		//send the request to the server
			//fetch(URL,{options})
		fetch(`http://localhost:3007/api/users/email-exists`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					firstName: fn,
					lastName: ln,
					email: email,
					password: pw
				})
		})
		.then( result => result.json())
		.then( result => {
			//console.log(result) //boolean
			if (result === false){
				//console.log(result)
				//if email not existing in the DB, send a request to register the user

				fetch(`http://localhost:3007/api/users/register`, {
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						}
						body: JSON.stringify({
							//user input
							firstName: fn,
							lastName: ln,
							email: email,
							password: pw
						})
				})
				.then(result => result.json())
				.then(result => {
					//console.log(result) //boolean

					if(result){
						alert(`User successfully registered!`)

						window.location.replace(`./login.html`)
					} else {
						alert(`Please try again`)
					}
				})

			} else {
				alert(`User already exists`)
			}
		})
	}
})



//get values of input fields

//send the request to the server